<?php
namespace VEB\VebOvrFemanager\Controller;

class NewController extends \In2code\Femanager\Controller\NewController {

    /**
     * action create
     *
     * @param VEB\VebOvrFemanager\Domain\Model\User $user
     * @validate $user In2code\Femanager\Domain\Validator\ServersideValidator
     * @validate $user In2code\Femanager\Domain\Validator\PasswordValidator
     * @return void
     */
    public function createAction(\VEB\VebOvrFemanager\Domain\Model\User $user) {
        parent::createAction($user);
    }
}