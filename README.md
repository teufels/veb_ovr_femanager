![VEB](https://img.shields.io/badge/VEB-Virtual_Event_Base-FBD75E.svg)
![VENDOR](https://img.shields.io/badge/vendor-VEB-219A83.svg)
![KEY](https://img.shields.io/badge/key-veb__ovr__femanager-blue.svg)
![version](https://img.shields.io/badge/version-0.0.2-yellow.svg?style=flat-square)
![depends](https://img.shields.io/badge/depends-in2code/femanager-red.svg?style=flat-square)

# addons & basic templating/configuration for feuser and femanager

## Changelog
- 0.0.4 basic templating/configuration
- 0.0.3 Add "About me, Department" to user fields
- 0.0.2 Add Twitter, Facebook, Instagram LinkedIn Profiles & Skype ID to user fields
- 0.0.1 Build Extension to expand fe_user & femanager

## ToDo:
- Add Twitter, Instagram, Facebook Profiles to user fields

## Documentation
https://docs.typo3.org/p/in2code/femanager/master/en-us/Features/NewFields/Index.html