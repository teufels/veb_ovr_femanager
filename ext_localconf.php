<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function () {

    // Register extended domain class
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \In2code\Femanager\Domain\Model\User::class,
            \VEB\VebOvrFemanager\Domain\Model\User::class
        );

    // Register extended service class -> not needed yet because bug could not be resolved by override
    /*
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \In2code\Femanager\Domain\Service\SendMailService::class,
            \VEB\VebOvrFemanager\Domain\Service\SendMailService::class
        );
    */

    // Register extended controller class -> not needed yet because still uses default backend templates
    /*
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            In2code\Femanager\Controller\UserBackendController::class,
            \VEB\VebOvrFemanager\Controller\UserBackendController::class
        );
    */

    // Hook for changing output before showing it
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][]
        = \VEB\VebOvrFemanager\Hooks\ContentPostProc::class . '->run';

    // Tasks
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['VEB\\VebOvrFemanager\\Task\\SendConfirmationTask'] = [
        'extension' => 'veb_ovr_femanager',
        'title' => 'LLL:EXT:veb_ovr_femanager/Resources/Private/Language/locallang.xlf:tx_VebOvrFemanager_domain_model_user.sendConfirmationTask.title',
        'description' => 'LLL:EXT:veb_ovr_femanager/Resources/Private/Language/locallang.xlf:tx_VebOvrFemanager_domain_model_user.sendConfirmationTask.description',
    ];
});