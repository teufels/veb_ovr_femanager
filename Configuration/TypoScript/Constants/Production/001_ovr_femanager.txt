plugin.tx_femanager {
	persistence {
		# cat=plugin.tx_femanager//a; type=int+; label= Default storage PID
		storagePid =
	}
	settings {
		# You need to specify the pid, where your main typoscript settings (frontend) is stored. Usually you will take the root page of your installation.
		configPID = 1

        # page uid for user profile
        userProfileUID =

        # page uid for edit profile
        editProfileUID =

        # page uid logout on entry
        logoutUID =

        # page uid of helpdesk
        helpdeskUID =

		# cat=plugin.tx_femanager//0100; type=text; label= Admin Name: Default admin name for all emails to the user
		adminName = Femanager

		# cat=plugin.tx_femanager//0101; type=text; label= Admin Email: Default admin email for all emails to the user
		adminEmail = Femanager@domain.org

		# cat=plugin.tx_femanager//0200; type=text; label= Upload folder: Define where to save images of the users
		uploadFolder = fileadmin/users/

		# cat=plugin.tx_femanager//0900; type=boolean; label= Include jQuery: Load and implement jQuery from external source (googleapis.com)
		jQuery = 0

		# cat=plugin.tx_femanager//0910; type=boolean; label= Include Twitter Bootstrap JS: Load and implement Twitter Bootstrap JavaScript from external source (bootstrapcdn.com)
		bootstrap = 0

		# cat=plugin.tx_femanager//0920; type=boolean; label= Include Twitter Bootstrap CSS: Load and implement Twitter Bootstrap CSS from external source (bootstrapcdn.com)
		bootstrapCSS = 0

		# cat=plugin.tx_femanager//0930; type=int+; label= Page ID for the resend email confirmation view
		showResendUserConfirmationRequestView = 0
	}
}